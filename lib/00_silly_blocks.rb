def reverser
  yield.split(" ").map {|word| word.reverse}.join(" ")
end

def adder(b = 1 )
  yield + b

end

def repeater(n = 0 )
  yield if n == 0
  n.times { |n| yield }
end
